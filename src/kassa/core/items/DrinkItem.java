package kassa.core.items;

/**
 * Drink Item
 * 
 * @author Stephen
 */
public class DrinkItem extends Item {

	private ItemFactory m_factory;

	/**
	 * Constructor
	 */
	public DrinkItem(int id, String name, double price, String subCat, ItemFactory factory, int tickets) {
		super(name, price, tickets, id);
		m_subCat = subCat;
		m_factory = factory;
	}

	/**
	 * Return category number
	 */
	@Override
	public String getCategory() {
		return "Drank";
	}

	@Override
	public String getColor() {
		return m_factory.getSubTypeColor(m_subCat);
	}

	@Override
	public String getSubCat() {
		return m_subCat;
	}
}
