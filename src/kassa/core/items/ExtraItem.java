package kassa.core.items;

/**
 * Main Item
 * 
 * @author Stephen Pauwels
 */
public class ExtraItem extends Item {

	/**
	 * Constructor
	 */
	public ExtraItem(int id, String name, double price, int tickets) {
		super(name, price, tickets, id);
	}

	/**
	 * Return category number
	 */
	@Override
	public String getCategory() {
		return "Extra";
	}

	@Override
	public String getColor() {
		return "250,250,0";
	}

}
