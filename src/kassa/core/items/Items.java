package kassa.core.items;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import kassa.core.exceptions.ItemsException;
import kassa.core.storage.Storage;

/**
 * Class for managing items
 * 
 * @author Stephen Pauwels
 */
public class Items implements Iterable<Item>, Iterator<Item> {

	private int m_count;

	private Storage m_database;
	private ItemFactory m_factory;

	private ArrayList<Item> m_items;

	/**
	 * Constructor
	 */
	public Items(Storage db) {
		m_items = new ArrayList<Item>();
		m_database = db;
		m_factory = new ItemFactory();
	}

	/**
	 * Add item to list
	 * 
	 * @param item
	 *            Item to add to list
	 * @throws ItemsException
	 *             , SQLException
	 */
	public void addItem(Item item) throws ItemsException, SQLException {
		for (Item itemIt : m_items) {
			if (itemIt.getName().equals(item.getName()) && itemIt.getSupplement().equals(item.getSupplement())) {
				throw new ItemsException(item, "Name & supplement already exists!");
			}
		}
		m_items.add(item);
		int id = m_database.addItem(item);
		item.setDatabaseId(id);
		sort();
	}

	public void addItems(Items items) throws ItemsException {
		for (Item item : items) {
			m_items.add(item);
		}
	}

	/**
	 * Add item to list
	 * 
	 * @param item
	 *            Item to add to list
	 * @throws ItemsException
	 *             , SQLException
	 */
	public void addLoadedItem(Item item) throws ItemsException {
		for (Item itemIt : m_items) {
			if (itemIt.getName().equals(item.getName())) {
				throw new ItemsException(item, "Name already exists!");
			}
		}
		m_items.add(item);
		sort();
	}

	public void clearAll() throws SQLException {
		m_items.clear();
		m_database.deleteItems();
	}

	/**
	 * Remove the item from the list
	 * 
	 * @param item
	 */
	public void deleteItem(Item item) throws SQLException {
		m_database.deleteItem(item);
		m_items.remove(item);
	}

	/**
	 * Return deserts
	 * 
	 * @return ArrayList<Item>
	 */
	public ArrayList<Item> getDeserts() {
		ArrayList<Item> list = new ArrayList<Item>();
		for (Item item : m_items) {
			if (item.getCategory().compareTo("Desert") == 0)
				list.add(item);
		}
		return list;
	}

	/**
	 * Return drinks
	 * 
	 * @return ArrayList<Item>
	 */
	public ArrayList<Item> getDrinks() {
		ArrayList<Item> list = new ArrayList<Item>();
		for (Item item : m_items) {
			if (item.getCategory().compareTo("Drank") == 0)
				list.add(item);
		}
		return list;
	}
	
	public ArrayList<String> getSupplements() {
		ArrayList<String> list = new ArrayList<String>();
		for (Item item : m_items) {
			if (item.getSupplement() != null && !item.getSupplement().equals(""))
				list.add(item.getSupplement());
		}
		return list;
	}

	/**
	 * Return item on index
	 * 
	 * @param index
	 *            Item to return
	 * @return Item
	 * @throws ItemsException
	 */
	public Item getItem(int index) throws ItemsException {
		if (index > m_items.size() - 1)
			throw new ItemsException(null, "Index out of bounds!");
		return m_items.get(index);
	}

	/**
	 * Return item by name
	 * 
	 * @param name
	 *            Item to return
	 * @return Item
	 */
	public Item getItemByName(String name) {
		for (Item item : m_items) {
			if (item.getName().equals(name))
				return item;
		}
		return null;
	}
	
	public Item getItemByID(int id) {
		for (Item item : m_items) {
			if (item.getDatabaseId() == id)
				return item;
		}
		return null;
	}

	/**
	 * Return mains
	 * 
	 * @return ArrayList<Item>
	 */
	public ArrayList<Item> getMains() {
		ArrayList<Item> list = new ArrayList<Item>();
		for (Item item : m_items) {
			if (item.getCategory().compareTo("Hoofdgerecht") == 0)
				list.add(item);
		}
		return list;
	}

	public ArrayList<Item> getNonDrinks() {
		ArrayList<Item> list = new ArrayList<Item>();
		for (Item item : m_items) {
			if (item.getCategory().compareTo("Drank") != 0)
				list.add(item);
		}

		return list;
	}

	/**
	 * Return the number of Items in the list
	 * 
	 * @return int
	 */
	public int getNrItems() {
		return m_items.size();
	}

	/**
	 * Return starters
	 * 
	 * @return ArrayList<Item>
	 */
	public ArrayList<Item> getStarters() {
		ArrayList<Item> list = new ArrayList<Item>();
		for (Item item : m_items) {
			if (item.getCategory().compareTo("Voorgerecht") == 0)
				list.add(item);
		}
		return list;
	}

	@Override
	public boolean hasNext() {
		return m_count < m_items.size();
	}

	@Override
	public Iterator<Item> iterator() {
		m_count = 0;
		return this;
	}

	private ArrayList<Item> merge(ArrayList<Item> lower, Item item, ArrayList<Item> upper) {
		ArrayList<Item> merged = new ArrayList<Item>();
		for (Item it : lower) {
			merged.add(it);
		}
		merged.add(item);
		for (Item it : upper) {
			merged.add(it);
		}
		return merged;
	}

	@Override
	public Item next() {
		if (m_count == m_items.size()) {
			return null;
		}
		return m_items.get(m_count++);
	}

	private ArrayList<Item> quickSort(ArrayList<Item> list) {
		ArrayList<Item> sorted = new ArrayList<Item>();
		if (list.size() == 0) {
			return sorted;
		} else if (list.size() == 1) {
			sorted.add(list.get(0));
			return sorted;
		} else {
			ArrayList<Item> lower = new ArrayList<Item>();
			ArrayList<Item> higher = new ArrayList<Item>();

			Item pivot = list.get((int) Math.floor(list.size() / 2));
			for (Item it : list) {
				if (it != pivot) {
					if (m_factory.sort(it, pivot) < 0) {
						lower.add(it);
					} else {
						higher.add(it);
					}
				}
			}
			return merge(quickSort(lower), pivot, quickSort(higher));
		}
	}

	@Override
	public void remove() {

	}

	public void sort() {
		m_items = quickSort(m_items);
	}

}
