package kassa.core.items;

/**
 * Main Item
 * 
 * @author Stephen Pauwels
 */
public class MainItem extends Item {
	
	private String m_supplements;
	
	/**
	 * Constructor
	 */
	public MainItem(int id, String name, double price, String supplement, int tickets) {
		super(name, price, tickets, id);
		if (supplement == null)
			m_supplements = "";
		else
			m_supplements = supplement;
	}

	/**
	 * Return category number
	 */
	@Override
	public String getCategory() {
		return "Hoofdgerecht";
	}

	@Override
	public String getColor() {
		return "0,255,0";
	}
	
	public int getNrTickets() {
		return super.getNrTickets();
	}
	
	public String getSupplement() {
		return m_supplements;
	}

}
