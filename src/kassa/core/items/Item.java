package kassa.core.items;

/**
 * Class for managing one item
 * 
 * @author Stephen Pauwels
 */
public class Item implements Comparable<Item> {

	protected String m_name;
	private double m_price;
	private int m_tickets;
	protected String m_subCat;
	private int m_database_id;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            Name of the item
	 * @param price
	 *            Price for the item
	 */
	public Item(String name, double price, int tickets, int database_id) {
		m_name = name;
		m_price = price;
		m_tickets = tickets;
		m_subCat = "";
		m_database_id = database_id;
	}
	
	public Item(String name, double price, int tickets) {
		m_name = name;
		m_price = price;
		m_tickets = tickets;
		m_subCat = "";
		m_database_id = -1;
	}
	
	public String toString() {
		return m_name + ": " + m_price;
	}
	
	public void setDatabaseId(int id) {
		if (m_database_id == -1)
			m_database_id = id;
	}
	
	public int getDatabaseId() {
		return m_database_id;
	}

	@Override
	public int compareTo(Item item) {
		return m_name.compareTo(item.m_name);
	}

	/**
	 * Return category number
	 */
	public String getCategory() {
		return "";
	}

	public String getColor() {
		return "";
	}

	/**
	 * Return the name of the item
	 * 
	 * @return String
	 */
	public String getName() {
		return m_name;
	}

	/**
	 * Return the price of the item
	 * 
	 * @return double
	 */
	public double getPrice() {
		return m_price;
	}

	public String getSubCat() {
		return "";
	}

	public void update(String name, double price, String subCat) {
		m_name = name;
		m_price = price;
		m_subCat = subCat;
	}
	
	public int getNrTickets() {
		return m_tickets;
	}
	
	public String getSupplement() {
		return "";
	}
}
