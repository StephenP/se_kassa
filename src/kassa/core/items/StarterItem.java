package kassa.core.items;

/**
 * Starter Item
 * 
 * @author Stephen Pauwels
 */
public class StarterItem extends Item {

	/**
	 * Constructor
	 */
	public StarterItem(int id, String name, double price, int tickets) {
		super(name, price, tickets, id);
	}

	/**
	 * Return category number
	 */
	@Override
	public String getCategory() {
		return "Voorgerecht";
	}

	@Override
	public String getColor() {
		return "0,0,255";
	}

}
