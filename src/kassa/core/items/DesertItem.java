package kassa.core.items;

/**
 * Desert Item
 * 
 * @author Stephen
 */
public class DesertItem extends Item {

	/**
	 * Constructor
	 */
	public DesertItem(int id, String name, double price, int tickets) {
		super(name, price, tickets, id);
	}

	/**
	 * Return category name
	 */
	@Override
	public String getCategory() {
		return "Desert";
	}

	@Override
	public String getColor() {
		return "255,0,0";
	}

}
