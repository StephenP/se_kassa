package kassa.core.items;

/**
 * Factory class for creating items
 * 
 * @author Stephen Pauwels
 */
public class ItemFactory {

	private String[] m_subTypeColors;

	private String[] m_subTypeNames;

	private String[] m_typeNames;

	public ItemFactory() {
		m_typeNames = new String[] { "Voorgerecht", "Hoofdgerecht", "Desert",
				"Drank", "Extra" };
		m_subTypeNames = new String[] { "Frisdrank", "Bier", "Sterke drank",
				"Warme drank", "Aperitief", "Snack" };
		m_subTypeColors = new String[] { "255,100,0", "72,118,255",
				"250,250,250", "0,205,102", "238,0,0", "205,205,0" };
	}

	public int catNr(String cat) {
		int index = 0;
		while (m_typeNames[index].compareToIgnoreCase(cat) != 0) {
			index++;
		}
		return index;
	}

	public Item createItem(int id, String itemType, String name, double price, String subCat, String supplement, int tickets) {
		if (itemType.compareTo("Voorgerecht") == 0)
			return new StarterItem(id, name, price, tickets);
		else if (itemType.compareTo("Hoofdgerecht") == 0)
			return new MainItem(id, name, price, supplement, tickets);
		else if (itemType.compareTo("Desert") == 0)
			return new DesertItem(id, name, price, tickets);
		else if (itemType.compareTo("Drank") == 0)
			return new DrinkItem(id, name, price, subCat, this, tickets);
		else if (itemType.compareTo("Extra") == 0)
			return new ExtraItem(id, name, price, tickets);
		else
			return new Item(name, price, tickets, id);
	}
	
	public Item createItem(String itemType, String name, double price, String subCat, String supplement, int tickets) {
		if (itemType.compareTo("Voorgerecht") == 0)
			return new StarterItem(-1, name, price, tickets);
		else if (itemType.compareTo("Hoofdgerecht") == 0)
			return new MainItem(-1, name, price, supplement, tickets);
		else if (itemType.compareTo("Desert") == 0)
			return new DesertItem(-1, name, price, tickets);
		else if (itemType.compareTo("Drank") == 0)
			return new DrinkItem(-1, name, price, subCat, this, tickets);
		else if (itemType.compareTo("Extra") == 0)
			return new ExtraItem(-1, name, price, tickets);
		else
			return new Item(name, price, tickets, -1);
	}

	protected String getSubTypeColor(String subCat) {
		return m_subTypeColors[subCatNr(subCat)];
	}

	public String[] getSubTypes() {
		return m_subTypeNames;
	}

	public String[] getTypes() {
		return m_typeNames;
	}

	public int sort(Item item1, Item item2) {
		if (item1.getCategory().compareTo(item2.getCategory()) == 0) {
			if (item1.getSubCat().compareTo(item2.getSubCat()) == 0) {
				return item1.getName().compareTo(item2.getName());
			} 
			return subCatNr(item1.getSubCat()) - subCatNr(item2.getSubCat());
		}
		return catNr(item1.getCategory()) - catNr(item2.getCategory());
	}

	public int subCatNr(String subCat) {
		int index = 0;
		while (m_subTypeNames[index].compareToIgnoreCase(subCat) != 0) {
			index++;
		}
		return index;
	}
}
